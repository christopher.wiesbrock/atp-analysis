# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 14:36:47 2024

@author: wiesbrock
"""

import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import numpy as np
import glob
import matplotlib.pyplot as plt
import pandas as pd
import os
import scipy.stats as stats
import seaborn as sns
from scipy.signal import argrelextrema
from tqdm import tqdm
from read_roi import read_roi_file
import math
from scipy.optimize import curve_fit
from sklearn.metrics import r2_score
import pandas as pd
from scipy.signal import butter, sosfreqz, sosfilt
from scipy.signal import find_peaks
from scipy.interpolate import InterpolatedUnivariateSpline

import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import uniform_filter1d

from pybaselines import Baseline
from pybaselines.utils import gaussian
from numba import jit, cuda 
#@jit(target_backend='cuda') 

from tqdm import tqdm

import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve

all_path=r'C:\Users\wiesbrock\Desktop\ATP\IMARIS traces\Testis_s\*'
all_path=glob.glob(all_path)

for i in tqdm(range(len(all_path))):
    file_list=glob.glob(all_path[i]+'/*')
    filtered_files = []

    for path in all_path:
        # Alle Dateien im aktuellen Pfad auflisten
        file_list = glob.glob(path + '/*')
    
        # Dateien filtern
        for file in file_list:
            if "Statistics" in file and "Pos" not in file:
                filtered_files.append(glob.glob(file+'/*')[0])

# In ein numpy Array umwandeln (nicht unbedingt notwendig)
#filtered_files = np.array(filtered_files)

index_number_t=[]
p=0
q=0

threshold=30

amp_t=[]
duration_t=[]
fwhm_values_t = []
fwhm_values_g=[]

# Lesen der CSV-Dateien
for file in filtered_files:
    df = pd.read_csv(file, skiprows=[0,1,2])
    names=df.columns
    names=names[1:-1]
    for names in names:
        x=np.linspace(0,len(df[names]),len(df[names]))
        baseline_fitter = Baseline(x_data=x)
        df[names] = df[names].fillna(df[names].mean())
        #df[names]=df[names]-baseline_fitter.noise_median(df[names])[0]
        indices=find_peaks(df[names],prominence=threshold)[0]
        border_left=find_peaks(df[names],prominence=threshold)[1]['left_bases']
        border_right=find_peaks(df[names],prominence=threshold)[1]['right_bases']
        if np.random.randint(0,1000,1)<=2:
            p=p+1
            plt.figure(dpi=300)
            plt.title('Testis')
            plt.plot(x,df[names])
            #plt.plot(x[indices],df[names][indices],'ro')
            plt.savefig(r'C:\Users\wiesbrock\Desktop\wiesbrock\Desktop\ATP Traces\uncorrected\testis'+str(p)+'.png')
        for left, right in zip(border_left, border_right):
            if right - left<=300 and right>left:
                amplitude = np.max(df[names][left:right]) - np.min(df[names][left:right])
                amp_t.append(amplitude/np.std(df[names]))
                duration_t.append(right-left)
                
        for i in range(len(indices)):
            peak_height = df[names][indices[i]]
            half_max = peak_height / 2
            
            left_base = border_left[i]
            right_base = border_right[i]
            
            roots=[]
            
            if right_base - left_base >= 3 and right_base - left_base < 200:
                spline = InterpolatedUnivariateSpline(x[left_base:right_base+1], df[names][left_base:right_base+1] - half_max)
                roots = spline.roots()
                
            else:
                fwhm_values_t.append(1)
            
            if len(roots) >= 2:
                fwhm = roots[-1] - roots[0]
                fwhm_values_t.append(fwhm)
                    
                
                
        index_number_t.append(len(indices))
        
all_path=r'C:\Users\wiesbrock\Desktop\ATP\IMARIS traces\Glass_s\*'
all_path=glob.glob(all_path)

for i in tqdm(range(len(all_path))):
    file_list=glob.glob(all_path[i]+'/*')
    filtered_files = []

    for path in all_path:
        # Alle Dateien im aktuellen Pfad auflisten
        file_list = glob.glob(path + '/*')
    
        # Dateien filtern
        for file in file_list:
            if "Statistics" in file and "Pos" not in file:
                filtered_files.append(glob.glob(file+'/*')[0])

# In ein numpy Array umwandeln (nicht unbedingt notwendig)
#filtered_files = np.array(filtered_files)

index_number_g=[]
amp_g=[]
duration_g=[]
fwhm_values_g=[]

# Lesen der CSV-Dateien
for file in filtered_files:
    df = pd.read_csv(file, skiprows=[0,1,2])
    names=df.columns
    names=names[1:-1]
    for names in names:
        x=np.linspace(0,len(df[names]),len(df[names]))
        baseline_fitter = Baseline(x_data=x)
        df[names] = df[names].fillna(df[names].mean())
        #df[names]=df[names]-baseline_fitter.noise_median(df[names])[0]
        indices=find_peaks(df[names],prominence=threshold)[0]
        border_left=find_peaks(df[names],prominence=threshold)[1]['left_bases']
        border_right=find_peaks(df[names],prominence=threshold)[1]['right_bases']
        if np.random.randint(0,1000,1)<=2:
            q=q+1
            plt.figure(dpi=300)
            plt.title('Glass')
            plt.plot(x,df[names])
            #plt.plot(x[indices],df[names][indices],'ro')
            plt.savefig(r'C:\Users\wiesbrock\Desktop\wiesbrock\Desktop\ATP Traces\uncorrected\glass'+str(q)+'.png')
        
        for left, right in zip(border_left, border_right):
            if right - left<=300 and right>left:
                amplitude = np.max(df[names][left:right]) - np.min(df[names][left:right])
                amp_g.append(amplitude/np.std(df[names]))
                duration_g.append(right-left)
                             
        for i in range(len(indices)):
            peak_height = df[names][indices[i]]
            half_max = peak_height / 2
            
            left_base = border_left[i]
            right_base = border_right[i]
            
            # Interpolation um genauere Positionen zu finden
            roots=[]
            if right_base - left_base >= 3 and right_base - left_base < 200:
                spline = InterpolatedUnivariateSpline(x[left_base:right_base+1], df[names][left_base:right_base+1] - half_max)
                roots = spline.roots()
                
            else:
                fwhm_values_g.append(1)
            
            if len(roots) >= 2:
                fwhm = roots[-1] - roots[0]
                fwhm_values_g.append(fwhm)
                    
                
                 
        index_number_g.append(len(indices))
        
#index_number_g=np.concatenate(index_number_g)
#index_number_t=np.concatenate(index_number_t)

hist_g,edges_g=np.histogram(index_number_g,bins=(0,1,2,3,4,5,6,7,8,9,10))
hist_t,edges_t=np.histogram(index_number_t,bins=(0,1,2,3,4,5,6,7,8,9,10))

cdf_g = np.cumsum(hist_g)
cdf_t = np.cumsum(hist_t)

# Normalize the CDF
cdf_g = cdf_g / cdf_g[-1]
cdf_t = cdf_t / cdf_t[-1]

# Plot the CDFs
plt.figure(dpi=300)
plt.plot(edges_g[:-1], cdf_g, label='Glass')
plt.plot(edges_t[:-1], cdf_t, label='Testis')
plt.xlabel('Number of peaks')
plt.ylabel('Cumulative Probability')
sns.despine()
plt.legend()
plt.savefig('cum_num.svg')

print((np.mean(amp_t)-np.mean(amp_g))/np.std(amp_g))
print(stats.ttest_ind(amp_t,amp_g))

data=amp_t,amp_g
plt.figure(dpi=300)
sns.violinplot(data=data, cut=0)
plt.xticks((0,1),['Testis','Glass'])
plt.ylabel('Amplitude[z-score]')
plt.xlabel('Group')
sns.despine()
plt.ylim(0,10)
plt.savefig('atp_amp.svg')

data=duration_t,duration_g
plt.figure(dpi=300)
sns.violinplot(data=data, cut=0)
plt.xticks((0,1),['Testis','Glass'])
plt.ylabel('Duration [Frames]')
plt.xlabel('Group')
sns.despine()
plt.savefig('atp_duration.svg')

print((np.mean(duration_t)-np.mean(duration_g))/np.std(duration_g))
print(stats.ttest_ind(duration_t,duration_g))

data=fwhm_values_t,fwhm_values_g
plt.figure(dpi=300)
sns.violinplot(data=data, cut=0)
plt.ylim(0,50)
plt.xticks((0,1),['Testis','Glass'])
plt.ylabel('FWHM [Frames]')
plt.xlabel('Group')
sns.despine()
plt.savefig('atp_fwhm.svg')

print((np.mean(fwhm_values_t)-np.mean(fwhm_values_g))/np.std(fwhm_values_g))
print(stats.ttest_ind(fwhm_values_t,fwhm_values_g))

        
    

    